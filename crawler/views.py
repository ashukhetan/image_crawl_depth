from django.shortcuts import render
from crawler.crawler import Crawler
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
import json

class GetImagesView(APIView):
    parser_classes = (JSONParser,)

    def post(self, request, format=None):
        depth = -1
        base_url = ''
        error_message = ''
        request_json = request.data
        if 'depth' in request_json:
            depth = int(request_json['depth'])
        else:
            error_message += 'Depth missing.'

        if 'base_url' in request_json:
            base_url = request_json['base_url']
        else:
            error_message += 'base url missing.'

        if error_message:
            return Response({'message': 'Failure', 'reason': error_message})

        try:
            crawl_ = Crawler(depth=depth, base_url=base_url)
            img_urls = crawl_.crawl()
            return Response({'message': 'Success', 'result': img_urls})
        except Exception as e:
            return Response({'message': 'Failure', 'reason': str(e)})
