from html.parser import HTMLParser
from urllib.request import urlopen


class Crawler(HTMLParser):
    def __init__(self, depth, base_url):
        HTMLParser.__init__(self)
        self.base_url = base_url
        self.depth = depth
        self.img_urls = dict()
        self.other_urls = {base_url: 0}
        self.current_depth = 0
        self.request_url = ''

    def handle_starttag(self, tag, attrs):
        tag = str(tag).lower()
        if (tag == 'a' or tag == 'img') and attrs:
            self.add_url_to_list(tag, attrs)

    def add_url_to_list(self, tag_required, attrs):
        if tag_required not in ['img', 'a']:
            return
        if tag_required == 'a':
            attr_name_required = 'href'
        elif tag_required == 'img':
            attr_name_required = 'src'

        for attr_name, attr_value in attrs:
            if attr_name == attr_name_required:
                required_url = attr_value
                if attr_value.startswith('//') or attr_value.startswith('\\'):
                    continue
                if attr_value.startswith('/'):
                    required_url = self.base_url + attr_value

                if tag_required == 'img':
                    if self.request_url not in self.img_urls:
                        self.img_urls[self.request_url] = {required_url}
                    else:
                        self.img_urls[self.request_url].add(required_url)

                elif tag_required == 'a' and required_url.startswith(self.base_url) and required_url not in self.other_urls:
                    self.other_urls[required_url] = self.current_depth + 1

    def crawl(self):
        for depth in range(self.depth):
            self.current_depth = depth
            for url in {key: self.other_urls[key] for key in self.other_urls if self.other_urls[key] == depth}:
                try:
                    self.request_url = url
                    req = urlopen(url)
                    res = req.read()
                    self.feed(str(res))
                except:
                    self.reset()

        return self.img_urls

