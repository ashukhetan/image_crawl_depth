import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment} from '../environments/environment'
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  base_url : string = '';
  depth: number = 1;

  // img_urls = {'hhh': ['https://www.google.co.in/logos/doodles/2018/fathers-day-2018-5929703499104256-s.png']};
  img_urls = {};

  get_url_keys(){
    return Object.keys(this.img_urls);
  }

  onclick(){
    this.img_urls = {};
    const request_body = {'depth': this.depth, 'base_url': this.base_url};
    const header_body = new HttpHeaders({'Content-Type': 'application/json'});
    this.http.post(environment.service_url, request_body, {headers: header_body}).subscribe(data => {
      let message = data['message'];
      if(message === 'Success'){
        this.img_urls = data['result'];
      }
    })
  }

  constructor(private http: HttpClient){}
}
